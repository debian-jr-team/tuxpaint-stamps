Neptune—one of the ice giants in our solar system.
ca.utf8=Neptú, un dels gegants de glaç del sistema solar.
fr.utf8=Neptune--une des géantes glaciaire de notre système solaire.
ja.utf8=「かいおうせい」 たいようけいにある おおきな こおりの わくせいの ひとつです
pt.utf8=Neptuno, um dos gigantes de gelo do nosso sistema solar.
sq.utf8=Neptuni - njëri nga “gjigantët e akullt” në sistemin tonë diellor.
