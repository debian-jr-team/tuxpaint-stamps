#!/bin/bash

# Find any sound effect audio files (NOT the "*_desc[_*].ogg" ones)
# that have high bitrates and reduce them.

# Use FFMPEG's `ffprobe` to fetch info about the files,
# and `ffmpeg` to resample.

echo -n "BEFORE: "
du -s -h stamps

echo

declare -a errors

for i in `find stamps -name "*.ogg" -and -not -name "*_desc*.ogg" | sort`; do
  b=`ffprobe -v quiet -show_entries stream=bit_rate -of default=noprint_wrappers=1:nokey=1 "$i" 2> /dev/null`
  echo
  echo "$i -- $b"
  if test "$b" != ""; then \
    if [ $b -gt 32000 ]; then \
      ffmpeg -hide_banner -i "$i" -b:a 32k "$i"-32k.ogg; \
      if test "$?" != "0"; then \
        echo "THERE WAS AN ERROR"; \
        errors+=($i); \
      else \
        mv "$i"-32k.ogg "$i"; \
      fi \
    else \
      echo "Already <= 32000bps!"; \
    fi \
  else \
    echo "CANNOT DETERMINE BITRATE"; \
  fi
  echo "========================================================================"
done

echo

echo ERRORS:
echo $errors

echo

echo -n "AFTER: "
du -s -h stamps

