#!/bin/bash

# Find any descriptive audio file ("*_desc[_*].ogg") file that's stereo
# and convert it to mono

echo -n "BEFORE: "
du -s -h stamps

for i in `file \`find stamps -name "*_desc.ogg" -or -name "*_desc_*.ogg"\` | grep -i stereo | cut -d ':' -f 1`; do
  ffmpeg -i "$i" -ac 1 "$i"-mono.ogg
  mv "$i"-mono.ogg "$i"
done

echo -n "AFTER: "
du -s -h stamps
