#!/bin/sh

# Runs `scour` against all SVG stamps in Tux Paint Stamps package.
#
# That tool is described as an "SVG scrubber and optimizer":
#
#   Scour is a Python module that aggressively cleans SVG files,
#   removing a lot of unnecessary information that certain tools or
#   authors embed into their documents.  The goal of scour is to
#   provide an identically rendered image (i.e. a scoured document
#   should have no discernible visible differences from the original
#   file) while minimizing the file size.
#
# Invoke this tool from the top level of `tuxpaint-stamps`.
#
# 2021-08-29 - 2023-10-05

for i in `find stamps -name "*.svg"`
do
  ls -l "$i"

  scour "$i" tmp.svg
  mv tmp.svg "$i"

  ls -l "$i"

  echo ------------------------------------------------------------------------------
done

