#!/bin/sh

# Runs `pngout` against all PNG stamps in Tux Paint Stamps package.
# (Previously, we used `pngcrush`.)

# "PNGOUT produces extremely well compressed PNG images."
# It is available from http://www.jonof.id.au/kenutils.html
#
# Invoke this tool from the top level of `tuxpaint-stamps`.
#
# WARNING: `pngout` can be very slow, so this process can take a
# long time.  It is best to simply ensure that any new, or modified,
# PNG-based stamps have `pngout` ran against them before committing
# [back] to the `tuxpaint-stamps` repository.
#
# 2007-06-28 - 2023-10-05

for i in `find stamps -name "*.png"`
do
  ls -l $i
  pngout -q $i
  ls -l $i
  echo ------------------------------------------------------------------------------
done

