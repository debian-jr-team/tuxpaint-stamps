#!/bin/bash

# Find any descriptive audio file ("*_desc[_*].ogg") file that
# have high bitrates and reduce them.

# Use FFMPEG's `ffprobe` to fetch info about the files,
# and `ffmpeg` to resample.

echo -n "BEFORE: "
du -s -h stamps

for i in `find stamps -name "*_desc.ogg" -or -name "*_desc_*.ogg" | sort`; do
  b=`ffprobe -v quiet -show_entries stream=bit_rate -of default=noprint_wrappers=1:nokey=1 "$i" 2> /dev/null`
  echo
  echo "$i -- $b"
  if [ $b -gt 32000 ]; then \
    ffmpeg -hide_banner -i "$i" -b:a 32k "$i"-32k.ogg; \
    mv "$i"-32k.ogg "$i"; \
  else \
    echo "Already <= 32000bps!"; \
  fi
  echo "========================================================================"
done

echo -n "AFTER: "
du -s -h stamps

